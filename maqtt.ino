#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// MQTT settings
char mqttServer[]     = "servidor.com"; // Dominio del servidor mqtt
int  mqttPort         = 1883; // Puerto en el que se maneja el mqtt
char mqttClientName[] = "arduino_21022705"; // Nombre en el que se maneja
char mqttTopic[]      = "focos"; // Topic al que va a estar suscrito este dispositivo

const int ledPin      = 13;    // Pin del led/foco
char message_buff[100];
String ident = "21022705"; // Identificador del dispositivo dentro del topic
char todo[] = "21022705/0"; // Valor inicial en el webservice


WiFiClient client;

PubSubClient mqttClient(mqttServer, mqttPort, client); // Utilizamos la librerìa PubSubclient para conectar mqtt

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    message_buff[i] = (char)payload[i];
  }
  Serial.println();
  String msgString = String(message_buff);
  if(msgString == "1") {
    digitalWrite(13, HIGH);
  }else{
    digitalWrite(13, LOW);
  }
  Serial.println(msgString);
}

void setup() {
  EEPROM.begin(512);
  EEPROM.write(0,3);
  pinMode(13, OUTPUT);
  Serial.begin(115200);
  Serial.println();
  // Desconectamos de la red antes conectada
  WiFi.disconnect();
  delay(1000);
  // Escaneamos las redes y listamos las disponibles
  Serial.print("Redes cercanas encontradas  :");
  Serial.println(WiFi.scanNetworks());
  delay(500);
  Serial.println("Lista de nombre de redes…:");
  int n = WiFi.scanNetworks();
  for (int i = 0; i < n; i++) {
    Serial.println(WiFi.SSID(i));
  }
  Serial.println();
  // Conectamos a la red que queremos
  WiFi.begin("MI-RED", "mipassword"); // El primer parámetro es el SSID y el segundo es la contraseña
  Serial.println("Conectando...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println();
  Serial.print("Contectado a          : ");
  Serial.println(WiFi.SSID());
  Serial.print("Dirección IP asignada : ");
  Serial.println(WiFi.localIP());
  Serial.print("Dirección MAC asignada         : ");
  Serial.println(WiFi.macAddress());
  Serial.print("La intensidad de la señal es         : ");
  Serial.println(WiFi.RSSI());

  if (mqttClient.connect(mqttClientName)) {
    Serial.println(F("Conetado al mqtt :D"));
    mqttClient.subscribe(ident.c_str());
  } else {
    Serial.println(F("Conexión al mqtt fallida :("));
    while (true);
  }

  pinMode(ledPin, OUTPUT); // Ponemos a disposición el puerto del led/foco

  mqttClient.setCallback(callback); // Setea el callback para leer info.
}

void loop() {
  mqttClient.loop();
  if(!mqttClient.connected()) { //Si no se ha conectado, ejecuta lo de abajo
    mqttClient.connect(mqttClientName); // Conecta a mqtt
    mqttClient.subscribe(ident.c_str()); // Se suscribe con su ident
    mqttClient.setCallback(callback); // Escucha los datos del webservices
    mqttClient.publish(mqttTopic, todo); //Apaga el foco de inicio
  }
}
